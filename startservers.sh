#!/bin/bash
#run this script as root once
cp index3.html /var/www/html/index3.html
cp nginx.conf /etc/nginx/nginx.conf
sed -i "s|GDATESINSTALLDIR|$(pwd)|g" dates.service chat.service
cp chat.service /etc/systemd/system/chat.service
cp dates.service /etc/systemd/system/dates.service
systemctl daemon-reload
 systemctl enable nginx  
 systemctl enable chat 
 systemctl enable dates

 systemctl restart nginx  
 systemctl restart chat 
 systemctl restart dates