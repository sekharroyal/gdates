#!/usr/bin/env bash
# Run as root

#from letsencrypt.sh
snap install core
snap refresh core
apt-get remove certbot
snap install --classic certbot
ln -s /snap/bin/certbot /usr/bin/certbot
certbot --nginx

#other stuff
sed -i "s/http/https/g" index3.html