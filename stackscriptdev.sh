#!/bin/bash
# <UDF label="gdates_password" name="gdates_password" />
# <UDF label="gdates_domain_name" name="gdates_domain_name" default="XXX" />




if [ -n "$1" ]
 then
  GDATES_PASSWORD="$1"
fi

if [ -n "$2" ]
 then
  GDATES_DOMAIN_NAME="$2"
fi

if [ "$GDATES_DOMAIN_NAME" = "XXX" ]
  then
	GDATES_DOMAIN_NAME=`ifconfig eth0 2>/dev/null|awk '/inet / {print $2}'|sed 's/addr://'`
fi


yes | apt-get -o Acquire::ForceIPv4=true update
yes | apt-get -o Acquire::ForceIPv4=true install vim nginx zip unzip make graphicsmagick qmail g++
useradd -m -p $(openssl passwd -1 $GDATES_PASSWORD) -s /bin/bash marek
cd /home/marek
su -c  'cd /home/marek; \
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh |  bash && \
echo NVM_DIR="/home/marek/.nvm"  >> ~/.bashrc; \
NVM_DIR="/home/marek/.nvm";
source ~/.bashrc && \
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && \
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" ; \
export PATH=/home/marek/.yarn/bin:/home/marek/.yarn/bin:/home/marek/.nvm/versions/node/v12.18.1/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games >> ~/.bashrc &&
echo  PATH=/home/marek/.yarn/bin:/home/marek/.yarn/bin:/home/marek/.nvm/versions/node/v12.18.1/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games >> ~/.bashrc && \
source ~/.bashrc && \
nvm install 12.18.1 && \
git clone https://gitlab.com/owsikowski/gdates.git && \ 
nvm use node && \
cd gdates && \
npm install yarn -g && \

yarn global add knex-migrator grunt-cli ember-cli && \
yarn setup && \
cd messenger && \
npm install total.js && \
cd .. && \
cd theia && \
yarn && \
yarn theia build \
' marek
cd /home/marek/gdates 
sed -i "s/mirkomarko.pl/$GDATES_DOMAIN_NAME/g" ./core/shared/config/env/*.json *.conf *.html theia.service
cp index3.html /var/www/html/index3.html
cp nginx.conf /etc/nginx/nginx.conf
cp theia.service /etc/systemd/system/theia.service
ufw allow 3000/tcp
systemctl daemon-reload
systemctl enable nginx  
systemctl restart nginx 
systemctl enable theia
systemctl restart theia
#bash /home/marek/marcco/startservers.sh
