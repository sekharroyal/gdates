Tinder clone, try out developer edition:

git clone https://gitlab.com/owsikowski/gdates.git && cd gdates && docker-compose up

Goal of the project: We deliver a great backend, making a new frontend should be easy and cheap.