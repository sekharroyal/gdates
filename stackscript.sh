#!/bin/bash

# <UDF label="gdates_password" name="gdates_password" />

# <UDF label="gdates_domain_name" name="gdates_domain_name" default="XXX" />

if [ "$GDATES_DOMAIN_NAME" = "XXX" ]

  then

	GDATES_DOMAIN_NAME=`ifconfig eth0 2>/dev/null|awk '/inet / {print $2}'|sed 's/addr://'`

fi

if [ -n "$1" ]
 then
  GDATES_PASSWORD="$1"
fi

if [ -n "$2" ]
 then
  GDATES_DOMAIN_NAME="$2"
fi

yes | apt-get -o Acquire::ForceIPv4=true update

yes | apt-get -o Acquire::ForceIPv4=true install vim nginx zip unzip make graphicsmagick qmail

useradd -m -p $(openssl passwd -1 $GDATES_PASSWORD) -s /bin/bash marek

cd /home/marek

su -c  'cd /home/marek; \

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh |  bash; \

export NVM_DIR="$HOME/.nvm"; \

[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" && \

[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" && \

nvm install 12.18.1 && \

git clone https://gitlab.com/owsikowski/gdates.git && \ 

nvm use node && \

/home/marek/gdates/zipinstall.sh /home/marek/gdates/delivery/mirkomarko-package.zip /home/marek/marcco ' marek

cd /home/marek/marcco 

sed -i "s/mirkomarko.pl/$GDATES_DOMAIN_NAME/g" ./core/shared/config/env/*.json *.conf *.html 

bash /home/marek/marcco/startservers.sh