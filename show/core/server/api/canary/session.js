const Promise = require('bluebird');
const { i18n } = require('../../lib/common');
const errors = require('@tryghost/errors');
const models = require('../../models');
const auth = require('../../services/auth');
const api = require('./index');

const sha1 = require('js-sha1');
const fs = require('fs');
const uuid = require('uuid');

const ObjectId = require('bson-objectid');

const session = {
    read(frame) {
        /*
         * TODO
         * Don't query db for user, when new api http wrapper is in we can
         * have direct access to req.user, we can also get access to some session
         * inofrmation too and send it back
         */
        return models.User.findOne({ id: frame.options.context.user });
    },
    doMessengerstuff(stuff, passwd) {
        const post = {
            id: ObjectId.generate(),
            title: 'Post with email-only card',
            // slug: 'email-only-card',
            // mobiledoc: '{"version":"0.3.1","atoms":[],"cards":[["email",{"html":"<p>Hey {first_name \\"there\\"} {unknown}</p><p><strong>Welcome to your first Ghost email!</strong></p>"}],["email",{"html":"<p>Another email card with a similar replacement, {first_name, \\"see?\\"}</p>"}]],"markups":[],"sections":[[10,0],[1,"p",[[0,[],0,"This is the actual post content..."]]],[10,1],[1,"p",[]]]}',
            // html: '<p>This is the actual post content...</p>',
            // plaintext: 'This is the actual post content...',
            "authors": [{"id": stuff.serverData.id}],
            status: 'published'
            // uuid: stuff.serverData.id
        };
        



        fs.readFile('./messenger/databases/users.json', (err, data) => {
            if (err) console.log("dramatic error, could not open messenger database ",process.cwd() , err)
            else {

                let students = JSON.parse(data);
                const found = students.find(element => element['email'] == stuff.serverData.email && element['password'] == sha1(passwd));
                if (!found) {
                    const newMessengerUser = {
                        "id": uuid.v4(),
                        "name": stuff.serverData.name,
                        "email": stuff.serverData.email,
                        "password": sha1(passwd),
                        "recent": {},
        "unread": {},
        "status": "",
        "position": "q",
        "department": "q",
        "picture": "face",
        "channels": null,
        "blocked": false,
        "notifications": true,
        "sa": false,
        "online": false,
                        "linker": stuff.serverData.slug,
                        "theme": "dark",
                        "blacklist": {},
                        "unreadcount": 0
                    };
                    students.push(newMessengerUser);
                    let newData = JSON.stringify(students);
                    fs.writeFile('./messenger/databases/users.json', newData, (err) => {
                        if (err) console.log("dramatic error, could not open messenger database ", err);
                    });
                    models.Post.add(post,{context: {internal: true}} );
                }
            }
        });

    },
    add(frame) {
        const object = frame.data;

        if (!object || !object.username || !object.password) {
            return Promise.reject(new errors.UnauthorizedError({
                message: i18n.t('errors.middleware.auth.accessDenied')
            }));
        }

        return models.User.check({
            email: object.username,
            password: object.password
        }).then((user) => {
            return Promise.resolve((req, res, next) => {
                req.brute.reset(function (err) {
                    if (err) {
                        return next(err);
                    }
                    req.user = user;
                    
                    session.doMessengerstuff(user, object.password);
                    auth.session.createSession(req, res, next);
                });
            });
        }).catch(async (err) => {
            if (!errors.utils.isIgnitionError(err)) {
                throw new errors.UnauthorizedError({
                    message: i18n.t('errors.middleware.auth.accessDenied'),
                    err
                });
            }

            if (err.errorType === 'PasswordResetRequiredError') {
                await api.authentication.generateResetToken({
                    passwordreset: [{
                        email: object.username
                    }]
                }, frame.options.context);
            }

            throw err;
        });
    },
    delete() {
        return Promise.resolve((req, res, next) => {
            auth.session.destroySession(req, res, next);
        });
    }
};

module.exports = session;
